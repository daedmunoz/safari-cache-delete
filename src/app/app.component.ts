import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  errorMessage = '';
  status = '';
  
  url = 'https://firebasestorage.googleapis.com/v0/b/angular-ng-build-safari.appspot.com/o/largeExample.mp3?alt=media&token=a8e90d6b-d591-4a0e-8bfa-79ec0c90cd92';
  id = "myAudioFile";

  retrievedUrl;
  constructor(private sanitizer: DomSanitizer) {

  }

  ngOnInit() {
    this.checkFileInCache();
  }

  checkFileInCache() {
    this.resetValues();
    return caches.has(this.id).then((inCache) => {
      console.log('In cache', inCache);
      this.status = inCache ? 'File in cache' : 'File not in cache';
    }).catch((error: Error) => {
      this.errorMessage = error.message;
      console.error(error);
    })
  }

  resetValues() {
    this.errorMessage = '';
    this.status = '';
    this.retrievedUrl = null;
  }

  saveFile() {
    this.resetValues();  
    caches.open(this.id).then((cache) => {
      console.log("open complete", cache);
      return fetch(this.url).then((response) => {
        console.log("fetch complete", response);
        return cache.put(this.url, response).then((data) => {
          console.log("cache complete", data)
          this.status = 'File saved';
        });
      })
    }).catch((error: Error) => {
      this.errorMessage = error.message;
      console.error('Error caching file!', error);
    });
  }

  removeFile() {
    this.resetValues();
    caches.delete(this.id).then((cacheDeleted) => {
      console.log("File removed from cache:", cacheDeleted);
      this.status = 'File removed';
    }).catch((error: Error) => {
      this.errorMessage = error.message;
      console.error('Error removing file from cache :(', error);
    });
  }

  retrieveCachedFile() {
    this.resetValues();    
    const noCacheErrorMessage = 'No Cache';

    return caches.has(this.id).then((inCache) => {
      console.log('In cache', inCache);

      if (inCache) {
        return caches.open(this.id);
      } else {
        return Promise.reject(new Error(noCacheErrorMessage));
      }
    }).then((cache) => {
      return cache.match(this.url);
    }).then((response) => {

      if (!response) {
        //no cached response, exit
        return Promise.reject(new Error(noCacheErrorMessage));
      }

      //if there's a response, we know that the audio has been cached before
      console.log("cache detected");

      return response.arrayBuffer();
    }).then((buffer) => {
      // do something with blob
      const blob = new Blob([new Uint8Array(buffer)], { type: 'audio/mpeg' });
      const blobUrl = URL.createObjectURL(blob);
      const trustedUrl = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
      this.status = 'Loaded from cache';
      this.retrievedUrl = trustedUrl;
      return '';
    }).catch((error: Error) => {
      console.error(error);
      this.errorMessage = error.message;
    });

  }
}
